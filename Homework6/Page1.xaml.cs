﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework6
{
    
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        async void getdict(string nentry)
        {
            
            var client = new HttpClient(); //allows us to get the api 
            var api = "https://owlbot.info/api/v2/dictionary/" + nentry; //this will get the api plus the word entered by the user in the entry box
            var uri = new Uri(api); //this will allow us to access the api and get the responses

            List<Definition> words = new List<Definition>(); //this will create a list made up of the definitions from the word entered by the user
            var response = await client.GetAsync(uri); //this will get the response from the api if available
            
            if(response.IsSuccessStatusCode)//checks to see if the response is valid
            {
                var content = await response.Content.ReadAsStringAsync(); //if the response gets through then the content will be saved as a string
                words = JsonConvert.DeserializeObject<List<Definition>>(content); //this is going to deserialize the json content that was recieved 
            }
            defwords.ItemsSource = words; //makes the listview itemsource to the json inserted in the words list
        }

        private async void nentry_Completed(object sender, EventArgs e)
        {
            if (internet.IsInternet())
            {
                string entered_word = nentry.Text; //this is going to save the entered user input

                if (!entered_word.All(Char.IsLetter)) //makes sure the letters entered are all characters with no digist entered
                {
                    await base.DisplayAlert("Error", "The entered word was spelled wrong or not in the dictionary", "OK");//Mispelled.IsVisible = true; //shows the label if the word entered was mispelled
                }

                entered_word = entered_word.ToLower();//this will make sure the entered word gets lowercased because the dictionary words are lowercased
                getdict(entered_word); //this will call the api owlbot to search for the inputed word
            }
            else
            {
                //if internet not working fine then display this
                await base.DisplayAlert("Internet Connection", "Please check internet Connection", "OK"); //if no internet connection can be found display will be shown
            }
        }
    }
}