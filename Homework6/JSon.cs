﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Homework6
{
    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; } //this is what is used to get the information was previously using "type"

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; } //this is used to get the information was previuosly using "definition"

        [JsonProperty("example")]
        public string Example { get; set; } //this is used to get the info was previously using "example"
    }
}
